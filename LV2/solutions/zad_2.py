# -*- coding: utf-8 -*-
"""
Created on Sat Nov 28 09:27:05 2020

@author: Mia
"""

import re


file = open("../resources/mbox-short.txt")

email_usernames = []

for line in file:
    usernames = re.findall('[0-9a-z]+[\._]?[0-9a-z]+[@]\w+', line)
    for username in usernames:
        email_usernames.append(username.split('@')[0])

user=' '.join(email_usernames)

at_least_one_a=re.findall('\S*a\S*',user)
print(at_least_one_a)

only_one_a=re.compile('^[^a]*a[^a]*$')
case2=list(filter(only_one_a.match,email_usernames))
print(case2)


without_a = re.compile('^[^a]+$')
case3 = list(filter(without_a.match,email_usernames))
print(case3)

one_or_more_numbers=re.findall('\S*[0-9]\S*',user)
print(one_or_more_numbers)

only_low_cases=re.findall('[a-z.]+',user)
print(only_low_cases)