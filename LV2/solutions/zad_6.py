# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 23:22:18 2020

@author: Mia
"""

import matplotlib.pyplot as plt

import matplotlib.image as mpimg

tiger=mpimg.imread('../resources/tiger.png')
tiger_plot=plt.imshow(tiger)

tiger*=1.8
tiger_plot=plt.imshow(tiger)
