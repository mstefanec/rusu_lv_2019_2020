# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 22:15:40 2020

@author: Mia
"""

import matplotlib.pyplot as plt

import pandas as pd

mtcars = pd.read_csv('../resources/mtcars.csv')

plt.scatter(mtcars.hp,mtcars.mpg,c=mtcars.wt,cmap='RdPu')
plt.colorbar().set_label('Mass[lbs]')
plt.xlabel('hp')
plt.ylabel('mpg')

print('Min. mpg: ', mtcars.mpg.min())
print('Max. mpg: ', mtcars.mpg.max())
print('Avg. mpg: ', sum(mtcars.mpg)/len(mtcars.mpg))
