# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 21:49:37 2020

@author: Mia
"""

import numpy as np

import matplotlib.pyplot as plt

die = np.random.randint(1,7,100)

plt.hist(die,bins = 80, color = 'darkorchid')

plt.xlabel('number')
plt.ylabel('appearances')