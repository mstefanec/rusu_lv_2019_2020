# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 20:32:30 2020

@author: Mia
"""

import numpy as np

import matplotlib.pyplot as plt

m_w = np.random.randint(2,size=180)

heights = np.ones(180,dtype=int)

heights[m_w == 0] = np.random.normal(167,7,len(heights[m_w == 0]))
heights[m_w == 1] = np.random.normal(180,7,len(heights[m_w == 1]))

plt.hist(heights[m_w == 0],bins = 80,color = 'r')
plt.hist(heights[m_w == 1],bins = 80,color = 'b')

def average_height_w():
    return sum(heights[m_w == 0]/len(heights[m_w == 0]))

def average_height_m():
    return sum(heights[m_w == 1]/len(heights[m_w == 1]))

plt.axvline(average_height_w(), color='hotpink')

plt.axvline(average_height_m(), color='darkorchid')

plt.legend(["avg_w","avg_m","w","m"])