# -*- coding: utf-8 -*-
"""
Created on Thu Dec  3 08:55:06 2020

@author: Mia
"""

import pandas as pd

mtcars = pd.read_csv('../resources/mtcars.csv')

#5 cars with highest mpg
mtcars_mpg_sorted=mtcars.sort_values(by=['mpg'],ascending=False)
print('5 cars with highest mpg: ',mtcars_mpg_sorted[:6])

#3 cars with 8 cyl and smallest mpg
print(mtcars_mpg_sorted[mtcars_mpg_sorted.cyl == 8].tail(3))

#average mpg for cars with 6 cyl
print(mtcars[mtcars.cyl == 6].mean().mpg)

#average mpg for cars with 4 cyl and wt between 2000 and 2200 lbs
print(mtcars[(mtcars.cyl == 4) & (mtcars.wt>=2.0) & (mtcars.wt<=2.2)].mean().mpg)

#number of cars with manual and number of cars with automatic gear
print('Cars with manual gear: ',len(mtcars[mtcars.am == 1]))
print('Cars with automatic gear: ',len(mtcars[mtcars.am == 0]))

#number of cars with automatic gear and hp>100
print('Number of cars with automatic gear and hp>100: ',len(mtcars[(mtcars.am==0) & (mtcars.hp>100)]))

#mass of cars in kg
print('Mass of cars in kg: ',mtcars.wt*1000*0.45359)


