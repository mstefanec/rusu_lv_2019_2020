# -*- coding: utf-8 -*-
"""
Created on Thu Dec  3 10:03:14 2020

@author: Mia
"""

import pandas as pd
import matplotlib.pyplot as plt

mtcars = pd.read_csv('../resources/mtcars.csv')

#barplot,mpg for cars with 4,6 and 8 cyl
#x-axes show car indexes with needed cylinder from the document
fig1,axes1=plt.subplots(figsize=(15,10),nrows=2,ncols=2)
fig1.tight_layout(pad=3.0)
for ax in axes1.flat:
    ax.set(ylabel='mpg')

#cyl==4
mtcars[mtcars.cyl == 4].mpg.plot.bar(ax=axes1[0][0],title='mpg for 4cyl',color='hotpink')

#cyl==6
mtcars[mtcars.cyl == 6].mpg.plot.bar(ax=axes1[0][1],title='mpg for 6cyl',color='magenta')

#cyl==8
mtcars[mtcars.cyl == 8].mpg.plot.bar(ax=axes1[1][0],title='mpg for 8cyl',color='darkorchid')

#
#boxplot,wt for cars with 4,6 and 8 cyl
fig2,axes2=plt.subplots(figsize=(15,10),nrows=2,ncols=2)
fig2.tight_layout(pad=3.0)
for ax in axes2.flat:
    ax.set(ylabel='weight')

#cyl==4
mtcars[mtcars.cyl == 4].wt.plot.box(ax=axes2[0][0],title='weight for 4cyl',color='hotpink')

#cyl==6
mtcars[mtcars.cyl == 6].wt.plot.box(ax=axes2[0][1],title='weight for 6cyl',color='magenta')

#cyl==8
mtcars[mtcars.cyl == 8].wt.plot.box(ax=axes2[1][0],title='weight for 8cyl',color='darkorchid')
plt.show()

#
#show which cars have higher mpg,automatic or manual
fig3= plt.figure(3)
ax = fig3.add_axes([0,0,1,1])
shifts = [mtcars[mtcars.am == 0].mpg, mtcars[mtcars.am == 1].mpg]
ax.set_title("Mpg for automatic and manual gear")
ax.set_ylabel("mpg")
ax.set_xlabel("Gear")
ax.set_xticklabels(['Automatic','Manual'])
fig3.show() #cars with manual gear have higher mpg

#
#qsec(acceleration) and hp of cars for manual and automatic gearbox
plt.figure(4)
plt.scatter(mtcars[mtcars.am==1].hp, mtcars[mtcars.am==1].qsec, color='hotpink' )
plt.scatter(mtcars[mtcars.am==0].hp, mtcars[mtcars.am==0].qsec, color='darkorchid')
plt.xlabel('Acceleration')
plt.ylabel('hp')
plt.legend(['Manual gear','Automatic gear'])



