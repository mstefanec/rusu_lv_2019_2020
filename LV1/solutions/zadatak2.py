# -*- coding: utf-8 -*-
"""
Created on Mon Nov 16 14:27:44 2020

@author: student
"""

try:
    ocjena=float(input("Unesite broj izmedju 0.0 i 1.0:"))
except ValueError: 
    print("Nije unijet broj!")
    
    
if ocjena < 0.0 or ocjena > 1.0:
    print("Ocjena je izvan zadanog intervala")
elif ocjena >= 0.9:
    print("Ocjena: A")
elif ocjena >= 0.8:
    print("Ocjena: B")
elif ocjena >= 0.7:
    print("Ocjena: C")
elif ocjena >= 0.6:
    print("Ocjena: D")
elif ocjena < 0.6:
    print("Ocjena: F")



