# -*- coding: utf-8 -*-
"""
Created on Mon Nov 16 14:54:51 2020

@author: student
"""

numbers=[]

while(True):
    number=input("Unesite broj: ")
    if number=="Done" or number=="done":
        break
    try:
        numbers.append(float(number))
    except ValueError:
        print("Nije unesen broj!")
    
print("Korisnik je unio %d brojeva" %(len(numbers)))

print("Srednja vrijednost: ",sum(numbers)/len(numbers))

print("Minimalna vrijednost: ", min(numbers))

print("Maksimalna vrijednost: ", max(numbers))
