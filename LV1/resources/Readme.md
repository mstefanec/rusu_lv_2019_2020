Ovaj repozitorij sadrži python skriptu koja broji koliko puta se pojedina riječ pojavljuje u tekstualnoj datatoteci. Rezultat bi trebao biti dictionary gdje uz svaku riječ (key) stoji broj ponavljanja riječi (value). Međutim, skripta ima bugove i ne radi kako je zamišljeno.

Promjene: Dodane zagrade kod naredbi print, fnamex->fname (5.linija), u else naredbi (17.linija) kod counts[word] dodano je +=1 kako bi se već postojećoj riječi pridodalo novo pojavljivanje

6. zadatak (opis rješenja)

	1. zadatak: vrijednost zarade ispisuje se preko varijable ukupnaZarada koja se dobije umnoškom varijabli radniSati te iznosPoSatu
	
	2. zadatak: prvo se izvrsava provjera je li broj uopće unesen, ako nije,ponovno se unosi, a ako je,provjerava se je li u dopuštenom intervalu te kojoj kategoriji ocjena pripada
	
	3. zadatak: definirala se funkcija total_kn koja prima dva parametra, radniSati i kn_h,a funkcija vraća njihov umnožak. Rezultat funkcije se ispisuje pomoću naredbe print
	
	4. zadatak: korisnik treba unositi brojeve sve dok ne unese riječ "Done", tada se prekida beskonačna petlja te se ispisuje koliko je brojeva korisnik unio te koji je najveći,najmanji i njihova srednja vrijednost. Ako nije unesen broj, ispisuje se odgovarajuća poruka
	
	5. zadatak: korisnik unosi ime datoteke u kojoj se provjeravaju linije i imaju li oblik "X-DSPAM-Confidence: <neki_broj>" (predstavljaju pouzdanost spam filtra). Ako postoji takva linija, varijabla pouzdanost se povećava za jedan (predstavlja broj linija koje sadrže pouzdanost),a varijabli zbroj_pouzdanosti se dodaje realni broj iz pronađene linije (koji se odvoji pomoću naredbe split()) te se nakon prolaska kroz datoteku izračuna srednja vrijednost pouzdanosti
	
	6. zadatak: korisnik unosi ime datoteke te se provjerava imali linija na početku riječ "From: ". Ako započinje,u listu email_list dodaje se email adresa iz te linije (pomoću naredbe split),a u dictionary domains se dodaje domena email adrese(dio iza @,ostvaruje se pomoću naredbe split) te se sprema broj njegovog pojavljivanja