# -*- coding: utf-8 -*-
"""
Created on Sat Jan  9 21:37:31 2021

@author: Mia
"""

from scipy.cluster.hierarchy import dendrogram, linkage
from sklearn import datasets, cluster
import numpy as np
import matplotlib.pyplot as plt

def generate_data(n_samples, flagc):
    if flagc == 1:
        random_state = 365
        X, y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)

    elif flagc == 2:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)

    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                                   cluster_std=[1.0, 2.5, 0.5, 3.0],
                                   random_state=random_state, centers=4)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)

    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)

    else:
        X = []

    return X


X = generate_data(257, 2)

grouped_data = linkage(X, method='average')
plt.figure()
dg = dendrogram(grouped_data)
plt.title('Dendrogram for dataset 2, average method')

grouped_data = linkage(X, method='centroid')
plt.figure()
dg = dendrogram(grouped_data)
plt.title('Dendrogram for dataset 2, centroid method')

grouped_data = linkage(X, method='ward')
plt.figure()
dg = dendrogram(grouped_data)
plt.title('Dendrogram for dataset 2, ward method')

grouped_data = linkage(X, method='median')
plt.figure()
dg = dendrogram(grouped_data)
plt.title('Dendrogram for dataset 2, median method')

grouped_data = linkage(X, method='single')
plt.figure()
dg = dendrogram(grouped_data)
plt.title('Dendrogram for dataset 2, single method')

plt.show()


