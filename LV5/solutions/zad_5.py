# -*- coding: utf-8 -*-
"""
Created on Sat Jan  9 22:28:02 2021

@author: Mia
"""

from sklearn import cluster
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg


new_img = mpimg.imread('../resources/example.png')
    
X = new_img.reshape((-1, 1))

plt.figure()
plt.title('Original image')
plt.imshow(new_img,  cmap='gray')

for clusters in range(2, 11):
    k_means = cluster.KMeans(n_clusters=clusters, n_init=1)
    k_means.fit(X) 
    values = k_means.cluster_centers_.squeeze()
    labels = k_means.labels_
    new_img_compressed = np.choose(labels, values)
    new_img_compressed.shape = new_img.shape
    
    plt.figure()
    plt.title('%d clusters' %clusters)
    plt.imshow(new_img_compressed,  cmap='gray')